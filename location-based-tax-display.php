<?php
/**
 * Plugin Name:     Location based tax display
 * Plugin URI:      https://gresak.net/wordpress
 * Description:     Displays the tax with or without tax based on location detected by browser language or GeoIP
 * Author:          Gregor Grešak
 * Author URI:      https://gresak.net
 * Text Domain:     location-based-tax-display
 * Domain Path:     /languages
 * Version:         1.0.0
 *
 * @package         Location_Based_Tax_Display
 */

//$_SERVER['GEOIP_COUNTRY_CODE'] = 'SI'; //for testing

LocationBasedTaxDisplay::instance();

class LocationBasedTaxDisplay
{
	protected $config;

	protected $current_user_country;

	protected $path;

	protected $incl = array();


	private static $instance;

	public function __construct($path = __FILE__) {
		$this->path = dirname($path);

		$this->config = array(
			"incl" => array("SI"),
			"excl" => array()
		);

		$this->current_user_country = isset($_SERVER['GEOIP_COUNTRY_CODE'])?$_SERVER['GEOIP_COUNTRY_CODE']:"";

		add_filter('pre_option_woocommerce_tax_based_on', array($this,'filter_tax_based_on'),10,2);
		add_filter('pre_option_woocommerce_tax_display_shop', array($this,'filter_display_option'),10,2);
		add_filter('pre_option_woocommerce_price_display_suffix', array($this,'filter_price_suffix'),10,2);

		add_filter('woocommerce_variable_price_html', array($this,'custom_variation_price'), 10, 2);
	}

	/**
	 * If prices are to be displayed with tax included on the shop front, 
	 * we probably don't know the shipping address, so the taxes would better
	 * be displayed based on the shopp address ( or the current user country code TODO )
	 * @param  [type] $option  [description]
	 * @param  [type] $default [description]
	 * @return [type]          [description]
	 */
	public function filter_tax_based_on($option, $default) {
		$result = false;
		if(in_array($this->current_user_country,$this->config['incl'])) {
			$result = 'base'; //options are 'base','shipping','billing'
		}

		return $result;
	}

	public function filter_display_option($option, $default) {

		$result = false;

		if(in_array($this->current_user_country,$this->config['incl'])) {
			$result = 'incl';
		} elseif (in_array($this->current_user_country,$this->config['excl'])) {
			$result = 'excl';
		}

		return $result; //returning false means take option from the DB
	}

	public function filter_price_suffix($option, $default) {
		$result = false;

		if(in_array($this->current_user_country,$this->config['incl'])) {
			$result = 'DDV vklj.';
		}

		return $result;

	}

	public function custom_variation_price( $price, $product ) {

		$suffix = get_option('woocommerce_price_display_suffix');

		$prices = $product->get_variation_prices('true');
		$min_price = current($prices['price']);

		return  "from ". wc_price($min_price)."<small class='woocommerce-price-suffix'> ".$suffix."</small>";

	}

	public static function instance($path = __FILE__) {

        if (self::$instance === null) {
            self::$instance = new self($path);
        }
        return self::$instance;
    }
}

